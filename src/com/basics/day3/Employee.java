package com.basics.day3;

public class Employee {
	private String ename, role, email,phone;
	
	
	public Employee() {
		System.out.println("Default Constructor is called...");
	}
	
	
	public Employee(String ename, String role, String email, String phone) {
		this.ename = ename;
		this.role = role;
		this.email = email;
		this.phone = phone;
	}


	public String getEname() {
		return ename;
	}


	public void setEname(String ename) {
		this.ename = ename;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	@Override
	public String toString() {
		return "Employee [ename=" + ename + ", role=" + role + ", email=" + email + ", phone=" + phone + "]";
	}
	
	
	
	
	
	
}
