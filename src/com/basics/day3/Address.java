package com.basics.day3;
/*
* Create a class Address with foll members
* country
* city
* pincode
* 
* default and parameterized constructor
* getters and setters
*/

public class Address {
	private String country;
	private String city;
	private int pincode;
	
// Default Constructor
	
	public Address()
	{
		country = "India";
		city = "Chennai";
		pincode = 600068;
	}
	
//Parameterized Constructor
	
	
	public Address(String country, String city, int pincode)
	{
		this.country = country;
		this.city = city;
		this.pincode = pincode;
	}
	
//Getters and Setters	
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	@Override
	public String toString() {
		return "Address [country=" + country + ", city=" + city + ", pincode=" + pincode + "]";
	}

	
}
