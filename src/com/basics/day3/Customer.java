package com.basics.day3;

public class Customer {
	private String cname,email,phone;
	private Address address;
	
	public Customer()
	{
		
	}
	public Customer(String cname, String email, String phone, Address address )
	{
		this.cname = cname;
		this.email=email;
		this.phone = phone;
		this.address = address;
	}
	

	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Customer [cname=" + cname + ", email=" + email + ", phone=" + phone + ", address=" + address + "]";
	}
	
	
}