package com.basics.day3;

/*
 * Constructors
 * 1) they are special methods used to initialize the instance members of a class
 * 2) they have name same as the class name
 * 3) they never return an value not even void
 * 4) they are called automatically when the object is created
 */

public class Constructor {
	private int radius;
	private String colour;
	
	public Constructor()
	{
		colour = "Red";
		radius = 2;
	}
	
	public Constructor(int r, String c)
	{
		colour = c;
		radius = r;
	}

}
